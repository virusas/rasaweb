ALTER TABLE `rasa_simple_story` 
CHANGE COLUMN `intent_code` `intent_code` VARCHAR(100) NOT NULL ,
CHANGE COLUMN `utter_code` `utter_code` VARCHAR(100) NOT NULL ;
