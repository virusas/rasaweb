CREATE TABLE `rasa_intent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `text` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `xover_ai`.`rasa_utter` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NULL,
  `text` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `xover_ai`.`rasa_simple_story` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `intent_code` VARCHAR(45) NOT NULL,
  `utter_code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
