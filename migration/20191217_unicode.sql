ALTER TABLE `rasa_intent` 
CHANGE COLUMN `text` `text` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ;

ALTER TABLE `rasa_utter` 
CHANGE COLUMN `text` `text` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ;
