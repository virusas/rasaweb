ALTER TABLE `rasa_intent` 
ADD COLUMN `rasaid` INT NULL AFTER `text`;

ALTER TABLE `rasa_simple_story` 
ADD COLUMN `rasaid` INT NULL AFTER `utter_code`;

ALTER TABLE `rasa_utter` 
ADD COLUMN `rasaid` INT NULL AFTER `text`;
