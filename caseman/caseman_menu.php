<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<div style="margin:10px;">
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="" style="">
			<img src="/rasaweb/img/logo.jpeg" width=200 height=140 >
			</a>
		</div>
	
	<!-- Divider -->
	<hr class="sidebar-divider my-0">
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?php if($_SERVER['REQUEST_URI'] == "/caseman.php"){ echo "active";} ?>">
	  <a class="nav-link" href="caseman.php">
	    <i class="fas fa-fw fa-tachometer-alt"></i>
	    <span>User</span></a>
	</li>
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?php if($_SERVER['REQUEST_URI'] == "/booking.php"){ echo "active";} ?>">
	  <a class="nav-link" href="booking.php">
	    <i class="fas fa-fw fa-tachometer-alt"></i>
	    <span>Booking</span></a>
	</li>
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?php if($_SERVER['REQUEST_URI'] == "/chat_list.php"){ echo "active";} ?>">
	  <a class="nav-link" href="chat_list.php">
	    <i class="fas fa-fw fa-tachometer-alt"></i>
	    <span>Chat</span></a>
	</li>
	
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item">
	  <a class="nav-link" href="logout.php">
	    <span>Logout</span></a>
	</li>
	
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
	  <button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>
<!-- End of Sidebar -->