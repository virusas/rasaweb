<?php
	session_start();
  require_once("DB_config.php");
  require_once("Role.php");
  require_once("checkLogin.php");

  //get selected intent..

  
  $originalStuff = [];
  $type = $_GET['type'];
  $id = "";
  $rating = "";
  $intent_id = "";
  $utter_id = "";

  $selected_id = "";
  if(isset($_GET["selected_id"])){
     $selected_id = $_GET["selected_id"];
  }


  if(isset($_GET["rating"])){
    $rating = $_GET["rating"];
    //echo "rating" . $rating;
  }


  if(isset($_GET["response"])){
    $response = $_GET["response"];
  }

  if (isset($_GET['id'])) {
    $id = $_GET['id'];
  }

  //update rating / intest
  if (isset($_GET['rating'])) {
    if (isset($_GET['selected_id'])){
      $selected_id = $_GET['selected_id'];
      //$intend_id = $_GET['intend_id'];
      
      if($type=="response"){
        $sql = "UPDATE icase_chat_record y SET utter_rating = '$rating' , utter_id = '$selected_id' , response = (select text from rasa_utter where code = '$selected_id') where id = $id";
      }else {
        $sql = "UPDATE icase_chat_record SET intent_rating = '$rating' , intent_id = '$selected_id'  where id = $id";
      }
    }else{
      if($type=="response"){
        $sql = "UPDATE icase_chat_record SET utter_rating = '$rating'  where id = $id";
      }else {
        $sql = "UPDATE icase_chat_record SET intent_rating = '$rating'  where id = $id";
      }
    }

    printSQL($sql, $debug);
    $xxxxx = $conn->query($sql);

  }

  //update learning table

  $intent_id = "";
  $intent_code = "";
  $intent_action = "";
  $intent_content = "";
  $utter_id = "";
  $cid = "";
  //get rating / insert 
  if (isset($_GET['id'])) {
    if($type=="response"){
      $sql = "SELECT utter_rating as rating, b.code as code , intent_id , utter_id ,  a.response as text FROM icase_chat_record a join rasa_utter b on a.utter_id = b.code where a.id  = '$id'";
    }else{
      $sql = "SELECT intent_rating as rating, b.code as code , intent_id, utter_id , a.content as text FROM icase_chat_record a join rasa_intent b on a.intent_id = b.code where a.id  = '$id'";
    }
    printSQL($sql, $debug);
    $rating = "";
    $chatHistoryResult = $conn->query($sql);
    if($chatHistoryResult){
      $chatHistoryResultArray = $chatHistoryResult->fetch_array();
      //while($row = $chatHistoryResultArray){
        $rating = $chatHistoryResultArray["rating"];
        $intent_id = $chatHistoryResultArray["intent_id"];
        $utter_id = $chatHistoryResultArray["utter_id"];
        if($selected_id==""){
          $selected_id = $chatHistoryResultArray["code"];
        }
      //}
    }
  }

    //create the story
    if (isset($_GET['selected_id']) && isset($_GET['rating']) && isset($intent_id) && isset($utter_id)) {
      $selected_id = $_GET['selected_id'];
      $new_utter_id = $utter_id;
      $new_intent_id = $intent_id;

      if($type=="response"){
        //$new_utter_id = $selected_id;  
        $sql = "UPDATE `rasa_simple_story` SET `utter_code`='$new_utter_id' where `intent_code`='$intent_id' and cid='$id'";
      }else{
        //$new_intent_id = $selected_id;
        $sql = "UPDATE `rasa_simple_story` SET `intent_code`='$new_intent_id' where `utter_code`='$utter_id' and cid='$id'";
      }
      //$sql = "UPDATE `rasa_simple_story` SET `intent_code`='$new_intent_id' , `utter_code`='$new_utter_id' where `intent_code`='$intent_id' and `utter_code`='$utter_id' and cid='$cid'";
      printSQL($sql, $debug);

      $updateResult = $conn->query($sql);
      $updateResultNum = $conn->affected_rows;
      
      //print_r("sql".$updateResult.$sql);
      //print($updateResultNum."gaga");
      if($updateResultNum == 0){
        if($type=="response"){
          $utter_id = $selected_id;  
        }else{
          $intent_id = $selected_id;
        }
        $sql = "INSERT INTO `rasa_simple_story` (`intent_code`, `utter_code`, `cid`) SELECT '$intent_id','$utter_id','$id' FROM DUAL  WHERE NOT EXISTS (select 1 from rasa_simple_story where `intent_code`='$intent_id'  and `utter_code`='$utter_id')";
        printSQL($sql, $debug);
        $xxxxx = $conn->query($sql);
        //print_r("sql2".$xxxxx.$sql);
      }
    }
    printSQL($sql, $debug);  

	if(isset($_POST['intent_action']) && isset($_POST['intent_code']) && isset($_POST['intent_content'])){

    $intent_code = $_POST['intent_code'];
    $intent_content = $_POST['intent_content'];
    $intent_action = $_POST['intent_action'];

		if($intent_action=="add"){
      if($type=="response"){
        $sql = "INSERT INTO `rasa_utter`(`code`, `text`) select '".$intent_code."','".$intent_content."' from dual WHERE NOT EXISTS (SELECT 1 FROM `rasa_utter` WHERE `code`='".$intent_code."' AND `text`='".$intent_content."') ";
        //$sql = "INSERT into rasa_utter values('','$loginname','$pass','1000','$supsystem','$name','$gender','$age','$phone')";
        $conn->query($sql);
      }else{
        $sql = "INSERT INTO `rasa_intent`(`code`, `text`) select  '".$intent_code."','".$intent_content."' from dual WHERE NOT EXISTS (SELECT 1 FROM `rasa_intent` WHERE `code`='".$intent_code."' AND `text`='".$intent_content."') ";
        //$sql = "INSERT into rasa_utter values('','$loginname','$pass','1000','$supsystem','$name','$gender','$age','$phone')";
      }
      printSQL($sql, $debug);
      $conn->query($sql);

		}else if($intent_action=="edit"){
      $intent_id = $_POST['intent_id'];
      if($type=="response"){
				$sql = "UPDATE `rasa_utter` SET `text`='$intent_content' WHERE id = ". $intent_id;
			}else{
				$sql = "UPDATE `rasa_intent` SET `text`='$intent_content' WHERE id = ". $intent_id;
			}
      printSQL($sql, $debug);
      $conn->query($sql);
    }else if($intent_action=="delete"){
      $intent_id = $_POST['intent_id'];
      if($type=="response"){
        $sql = "delete from `rasa_utter` WHERE id = ". $intent_id;
      }else{
        $sql = "delete from `rasa_intent`  WHERE id = ". $intent_id;
      }
      printSQL($sql, $debug);
      $conn->query($sql);
    }
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iCase Manager - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  
    <link rel="apple-touch-icon" href="logo192.png" />
    <link rel="manifest" href="/manifest.json" />
    <script type="text/javascript">
      function choose(num){
        if(num==0){
          $('.new-intent-wrapper').show();
        }else{
          $('.new-intent-wrapper').hide();
        }
      }

      function EditIntent(intent_id, intent_code, intent_action, intent_content){
        //alert('intent_id'+intent_id);
        //alert('intent_code'+intent_code);
        //alert('intent_action'+intent_action);
        //alert('intent_content'+intent_content);
        $('.intentModalTitle').text('Edit new Response');
        $('#btn-add').val('Edit');
        $("#intent_code").prop("readonly", true);
        $('#intent_id').val(intent_id);
        $('#intent_code').val(intent_code);
        $('#intent_action').val(intent_action);
        $('#intent_content').val(intent_content);
      }

      function DeleteIntent(id, action){
        $('#intent_id').val(id);
        $('#intent_action').val(action);
        if(confirm('Are you sure?')){
          document.getElementById("dr_form").submit();

        }
      }
      
      function AddIntent(){
        //alert('intent_id'+intent_id);
        //alert('intent_code'+intent_code);
        //alert('intent_action'+intent_action);
        //alert('intent_content'+intent_content);
        $('.intentModalTitle').text('Add new response');
        $('#intent_action').val('add');
        $('#btn-add').val('Add');
        $('#intent_id').val('');
        $('#intent_code').val('');
        $('#intent_content').val('');
        
      }

    </script>
    
</head>

<body id="page-top">
<div id="root"></div>
  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php require_once("caseman/caseman_menu.php"); ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
      <form method='GET' id="intentForm">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $post_loginname; ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><?=$type?>
              </h1>
              <br><!--
            <h1 class="h3 mb-0 text-gray-800">Time : 
              <?=$datetime?>
              </h1>-->
          </div>

     
          <div class="row">
            <div class="col-xl-6 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    <?php 
                      $_text = "Oringal ".$type;
                    ?>
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1"><?=$_text?></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?=$chatHistoryResultArray["code"]?> <br><br><?=$chatHistoryResultArray["text"]?> </div>
                        
                      <!--<form action="generate_rasa.php" >
                        <input type="submit" value="train"></input>
                      </form>-->
                      <!--<a href="/rasaweb/bot/generate_rasa.php" target="_blank">train</a>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>



          <div class="col-xl-6 col-md-6 mb-4" >
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Rating</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <div id="rating-wrapper" class="row" style="margin-top:10px;" >
                            <div class="col-xl-1" >
                            0
                              <input type="radio" id="rating-0" name="rating" value="0" onclick="choose('0');" <?php if($rating==0){ echo "checked=checked";}?></input>
                            </div>
                            <div class="col-xl-1" >
                            1
                              <input type="radio" id="rating-1"  name="rating" value="1" onclick="choose('1');" <?php if($rating==1){ echo "checked=checked";}?>></input>
                            </div>
                            <div class="col-xl-1" >
                            2
                            <input type="radio"  id="rating-2" name="rating" value="2" onclick="choose('2');" <?php if($rating==2){ echo "checked=checked";}?>></input>
                            </div>
                            <div class="col-xl-1" >
                            3
                            <input type="radio"  id="rating-3" name="rating" value="3" onclick="choose('3');" <?php if($rating==3){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" >
                            4
                            <input type="radio"  id="rating-4" name="rating" value="4" onclick="choose('4');" <?php if($rating==4){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" >
                            5
                            <input type="radio"  id="rating-5" name="rating" value="5" onclick="choose('5');" <?php if($rating==5){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" >
                            6
                            <input type="radio"  id="rating-6" name="rating" value="6" onclick="choose('6');" <?php if($rating==6){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" >
                            7
                            <input type="radio"  id="rating-7" name="rating" value="7" onclick="choose('7');" <?php if($rating==7){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" >
                            8
                            <input type="radio"  id="rating-8" name="rating" value="8" onclick="choose('8');" <?php if($rating==8){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" >
                            9
                            <input type="radio"  id="rating-9" name="rating" value="9" onclick="choose('9');" <?php if($rating==9){ echo "checked=checked";}?>></input>

                            </div>
                            <div class="col-xl-1" style="padding:2px;">
                            10
                            <input type="radio"  id="rating-10" name="rating" value="10" onclick="choose('10');" <?php if($rating==10){ echo "checked=checked";}?>></input>
                            </div>
                            <div class="col-xl-1" >
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="new-intent-wrapper" <?php if($rating>0){ ?> style="display:none;" <?php }?>>

            <!-- Pending Requests Card Example -->
            <div class="col-xl col-xl col-md mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Add <?=$type?></div>
                    </div>
                      <div class="col-auto">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#AddIntentModal"  onclick="AddIntent();">
                          <i class="fas fa-plus fa-2x text-gray-300"></i>
                        </a>
                      </div>
                  </div>
                </div>
              </div>
            </div>

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-warning">Choose new <?=$type?></h6>
                </div>
                <div class="card-body">
              		<div class="table-responsive">
                		<table class="table table-bordered" id="patient_dataTable" width="100%" cellspacing="0">
                  		<thead>
                    		<tr>
                      		<th>Code</th>
                      		<th>Sample Message</th>
                      		<th></th>
                      		<th></th>
                      		<th></th>
                    		</tr>
                  		</thead>
                  		<tbody>
                      <?php
                          $row_index = 1;
                          $conn->query("set character_set_client='utf8'"); 
                          $conn->query("set character_set_results='utf8'"); 
                          $conn->query("set collation_connection='utf8_general_ci'"); 
                          if($type=="intent"){
                            $sql = "select * from rasa_intent";
                          }else{
                            $sql = "select * from rasa_utter";
                          }
                          $result = $conn->query($sql);


                          $rownum = 1;
                          while($row = $result->fetch_array()){
                            $row_index += 1;
                            $code = $row['code'];
                            $text = $row['text'];
                            $current_id = $row['id'];
                            $rownum = $rownum + 1;
                    ?>
                    <!--
                        <tr  id="row<?=$row_index ?>">
                          <td width=30%>
                          utter_not_want_illness
                          </td>
                          <td width=30%>
                          若該感受能依據聖經的內容則是神的旨意，若違反了神的內容或干擾日常的生活，則可能涉及精神失調的範疇，建議盡早尋找精神科相關服務機構作出適當的評估。 
                          </td>
                          <td>
                          <a href="javascript:$('tr').css('background-color','white').css('color','#858796');$('#row<?=$rownum?>').css('background-color','grey').css('color','white');$('#selected').val('<?=$current_id?>')">Choose</a>
                          </td>
                        </tr>-->
                        <tr  id="row<?=$rownum?>" <?php if($code == $selected_id){ echo " style='background-color:#858796;color:white;' "; }?>>
                          <td width=30%>  
                          <?=$code?>
                          </td>
                          <td width=30%  >
                          <?=$text?>
                          </td>
                          <td>
                             <a href="javascript:$('tr').css('background-color','white').css('color','#858796');$('#row<?= $row_index?>').css('background-color','grey').css('color','white');$('#selected_id').val('<?=$code?>')">Choose</a>
                             <!--<input type="submit" value="choose"></input>-->
                          </td>
                          <td>
                                <a href="javascript:void(0);" onclick="EditIntent(<?=$current_id?>, '<?=$code?>', 'edit', '<?=$text?>');" data-toggle="modal" data-target="#AddIntentModal">
                                  Edit
                                </a>
                          </td>
                          <td>
                                <a href="javascript:void(0);" onclick="DeleteIntent(<?=$current_id?>, 'delete');">
                                  Delete
                                </a>
                          </td>
                        </tr>
                    <?php
                        }
                      ?>
											</tbody>
										</table>
                          <input id="id" name="id" type="hidden" value="<?=$id?>"></input>
                          <input id="selected_id" name="selected_id" type="hidden" value="<?=$selected_id?>"></input>
                          <input id="type" name="type" type="hidden" value="<?=$type?>"></input>
									</div>
                </div>
              </div>


        </div>
        <!-- /.container-fluid -->
      </div>
      <div class="row">
        <div class="col">
          <input type="submit" value="Save"></input>
        </div>
      </div>
      </form>

      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- AddBookingResponse-->
  <div class="modal fade" id="AddIntentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title intentModalTitle" id="exampleModalLabel">Add new <?=$type?></h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group row">
              <form id="dr_form" name="dr_form" action="" method="POST" style="width:100%;">
                <div class="modal-body">
                  <div class="form-group">
                      <input type="text" id="intent_code" name="intent_code" value="<?=$intent_code?>" class="form-control form-control-user" placeholder="<?php if($type=="response"){ echo "id e.g. utter_want_to_take_medicine"; }else{ echo "id e.g. want_to_take_medicine"; }?>" required>
                    <input type="hidden" id="intent_id" name="intent_id" value="<?=$intent_id?>" required>
                    <input type="hidden" id="intent_action" name="intent_action" value="add" required>
                  </div>
                  <div class="form-group">
                    <textarea type="text" id="intent_content" name="intent_content" value="<?=$intent_content?>" class="form-control form-control-user" placeholder="content e.g. 我唔想食藥" required style="height:300px;"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-primary" id="btn-add" value="Add">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
              </form>
          </div>
        </div>
      </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
$(document).ready(function() {
  $('#patient_dataTable').DataTable();
  var x =document.getElementsByClassName("dataTables_wrapper");
  for (i = 0; i < x.length; i++) {
  	 x[i].style.width = "99%";
  }
});

  </script>

</body>

</html>

