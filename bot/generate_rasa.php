<?php
    session_start();
    require_once("../DB_config.php");

    if(isset($_SERVER['HTTP_ORIGIN'])){
        header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        header("Access-Control-Allow-Headers: Content-Type");
    }

    $intents = array();

    $file_content = "";
    $last_code = "";
    $rasa_select_query = "";

    if(isset($_GET['rasaid'])) {
		$rasaid = (int)$_GET['rasaid'];
        $RASA_ENDPOINT = $RASA_ENDPOINTS[$rasaid];
        $rasa_select_query = "where rasaid = $rasaid";
	}

    $conn->query("set character_set_client='utf8'"); 
    $conn->query("set character_set_results='utf8'"); 
    $conn->query("set collation_connection='utf8_general_ci'");
    $result = $conn->query("select * from rasa_intent $rasa_select_query");
    $sql = "select * from rasa_intent $rasa_select_query";
    //print($sql);
    while($row = $result->fetch_array()){
        $code = $row['code'];
        $text = $row['text'];

        if ($code != $last_code) {
            array_push($intents, $code);

            if (!empty($last_code)) {
                $file_content .= "\n";
            }
            $last_code = $code;
            $file_content .= "## intent:$code\n";
        }
        $file_content .= "- $text\n";

    }

    $fp = fopen('nlu.md', 'w');
    fwrite($fp, $file_content);
    fclose($fp);
    $nlu_content = $file_content;
    // generated nlu.md

    $utters = array();

    $file_content = "";
    $last_code = "";

    $result = $conn->query("select * from rasa_utter $rasa_select_query");

    $file_content .= "intents:\n";
    foreach($intents as $intent) {
        $file_content .= "- $intent\n";
    }

    $file_content .= "templates:\n";
    while($row = $result->fetch_array()){
        $code = $row['code'];
        $text = $row['text'];

        if ($code != $last_code) {
            array_push($utters, $code);

            // if (!empty($last_code)) {
            //     $file_content .= "\n";
            // }
            $last_code = $code;
            $file_content .= "  $code:\n";
        }
        $file_content .= "  - text: $text\n";
    }

    $file_content .= "actions:\n";
    foreach($utters as $utter) {
        $file_content .= "- $utter\n";
    }

    $fp = fopen('domain.yml', 'w');
    fwrite($fp, $file_content);
    fclose($fp);
    $domain_content = $file_content;
    // generate domain.yml

    $file_content = "";
    $last_code = "";

    $result = $conn->query("select * from rasa_simple_story $rasa_select_query");

    // $file_content .= "intents:\n";
    // foreach($intents as $intent) {
    //     $file_content .= "- $intent\n";
    // }

    // $file_content .= "templates:\n";
    while($row = $result->fetch_array()){
        $intent_code = $row['intent_code'];
        $utter_code = $row['utter_code'];

        $file_content .= "## $intent_code\n";
        $file_content .= "* $intent_code\n";
        $file_content .= "    - $utter_code\n";
        $file_content .= "    - action_restart\n";
    }

    $fp = fopen('stories.md', 'w');
    fwrite($fp, $file_content);
    fclose($fp);
    $stories_content = $file_content;
    // generated stories.md


    //print("domain.".$domain_content);
    //print("nlu_content.".$nlu_content);
    //print("stories_content.".$stories_content);

    
    // $user_input = $_POST['message'];
    $url = $RASA_ENDPOINT.'/model/train';
    $ch = curl_init();
    $payload_data = array(
        "config" => "%YAML 1.1\n---\nlanguage: en\npipeline: supervised_embeddings\npolicies:\n- name: MemoizationPolicy\n- name: KerasPolicy\n- name: MappingPolicy\n- core_threshold: 0.2\n  fallback_action_name: action_default_fallback\n  name: FallbackPolicy\n  nlu_threshold: 0.1\n",
        "domain" => $domain_content,
        "nlu" => $nlu_content,
        "stories" => $stories_content,
        "force" => false,
        "save_to_default_model_directory" => true
    );


    //print_r($domain_content);
    $payload = json_encode( $payload_data );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //print_r($result);
    if ($httpcode == 200) {
        echo "Train successfullay!";
    } else {
        echo "Train fail!";
        // oops! have to handle it
    }
    $matches = [];

   
   //print($result);
    preg_match('/\d+-\d+\.tar/', $result, $matches);
    $model_file = $matches[0].".gz";
    // echo $result;
    // $content = json_decode($result);
    // $intent = $content->{'intent'}->{'name'};
    // $confidence = $content->{'intent'}->{'confidence'};

    // update model
    // oops! absolute path
    // $model_file = "/Users/efc/works/learn/rasaxx/models/20191212-214627.tar.gz";
    $model_file = "models/$model_file";

    $url = $RASA_ENDPOINT.'/model';
    $ch = curl_init();
    $payload_data = array(
        "model_file" => $model_file
    );
    $payload = json_encode( $payload_data );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    // curl_setopt($ch, CURLOPT_PUT, true);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($httpcode == 200 || $httpcode == 204) {
        echo "Update model";
    }
    else {
        // oops! have to handle it
    }

?>