<?php

	session_start();
	require_once("../DB_config.php");
	if(isset($_SERVER['HTTP_ORIGIN'])){
		header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
		header("Access-Control-Allow-Headers: Content-Type");
	}
	$login_fail = 0;
	$uid = 0;

	/*
	if(isset($_SESSION['loginname'])){
		$post_loginname = $_SESSION['loginname'];
		$sql = "select * from icase_user where loginname = '$post_loginname'";
		$result = $conn->query($sql);
		$row_num = $result->num_rows;
		if($row_num == 1){
			$row = $result->fetch_array();
			if($row['permission'] != 1000){
				$login_fail = 1;
			}else{
				$uid = $row['uid'];
			}
		}
	}else{
		$login_fail = 1;
	}*/

	//if($login_fail != 1 && isset($_POST['message'])){
	$booking_id = 0;
	$old_msg_id = 0;
	$role = 0;
	$isFirstLoad = true;

	if(isset($_GET['uid']) && $_GET['uid'] != 0){
		$uid = $_GET['uid'];
	}
		
	if(isset($_GET['booking_id']) && $_GET['booking_id'] != 0){
		$booking_id = $_GET['booking_id'];
	}
	
	if(isset($_GET['old_msg_id']) && $_GET['old_msg_id'] != 0){
		$isFirstLoad = false; 
		$old_msg_id = $_GET['old_msg_id'];
	}
	
	if(isset($_GET['role'])){
		$role = $_GET['role'];
	}

	$result = getMsg($conn, $booking_id, $uid, $old_msg_id, $isFirstLoad, $role);

	function getMsg($conn, $booking_id, $uid, $old_msg_id, $isFirstLoad , $role){

		$last_msg_id = 0;
		
		//$target_role = 1000;
		if($role==1000){
			$target_role=2000;
		}else if($role==2000){
			$target_role = 1000;
		}
		//$isFirstLoad = true; //for first load , will get all the record instead of just one.
		if(!$isFirstLoad){
			//do the long polling
			$sql = "";
			while($last_msg_id <= $old_msg_id) 
			{
				usleep(1000); 
				clearstatcache(); 
				$sql =  "SELECT id FROM icase_chat_record  where booking_id = " . $booking_id . " and role = ".$target_role." ORDER BY id DESC LIMIT 1";
				$result = mysqli_query($conn,$sql); 
				//echo json_encode($result);
				//echo $sql;
				while($row = mysqli_fetch_array($result)) 
				{
					if(!isset($old_msg_id)){
						$old_msg_id = $row['id'];
					}
					
					$last_msg_id = $row['id']; 
				}
			}

			//get details from the selected row
			$result = mysqli_query($conn, "SELECT id, content, response FROM icase_chat_record where id = " . $last_msg_id . "  ORDER BY id DESC LIMIT 1"); 
		
			//get just one data 
			while($row = mysqli_fetch_array($result))
			{ 
				$content = $row['content'];  
				$response_arr = $row['response']; 
			}
			
			$response = array(); 
			$response['content'] = $content; 
			$response['response'] = $response_arr;
			$response['id'] = $last_msg_id; 

			echo "[".json_encode($response)."]";

		}else{
			//load all
			//$sql = "SELECT id, content, response FROM icase_chat_record where booking_id = " . $booking_id . "  ORDER BY id DESC";
			$sql = "SELECT id, content, response FROM icase_chat_record where uid = " . $uid . "  ORDER BY id DESC";

			$result = mysqli_query($conn, $sql); 
			
			$rows = array();
			while($r = mysqli_fetch_assoc($result)) {
				$rows[] = $r;
			}
			echo json_encode($rows); 

		}
	}
?>