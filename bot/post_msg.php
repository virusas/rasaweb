<?php
	session_start();
	require_once("../DB_config.php");
	use Aws\Sns\SnsClient; 
	use Aws\Exception\AwsException;
	use Twilio\Rest\Client;

	function send_sms($msg, $phone) {
		// Your Account SID and Auth Token from twilio.com/console
		$account_sid = 'AC31429d70ea6cb1b891871b56881ca51d';
		$auth_token = '7aa8a76caf9e1b5040f0dd28da1a0f7e';
		// In production, these should be environment variables. E.g.:
		// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]

		// A Twilio number you own with SMS capabilities
		$twilio_number = "+14138873405";

		$client = new Client($account_sid, $auth_token);
		$client->messages->create(
			// Where to send a text message (your cell phone?)
			"+852 ".$phone,
			array(
				'from' => $twilio_number,
				'body' => $msg
			)
		);
	}

	function _send_sms($msg) {
		global $AWS_KEY;
		global $AWS_SECRET;
		global $SMS_PHONE_NUMBER;

		$SnSclient = new SnsClient([
			'region' => 'us-east-1',
			'version' => '2010-03-31',
			'credentials' => [
				'key' => $AWS_KEY,
				'secret' => $AWS_SECRET,
		  	]
		]);
		
		// $message = 'This message is sent from a Amazon SNS code sample.';
		$phone = "+852 ". $SMS_PHONE_NUMBER;
		
		try {
			$result = $SnSclient->publish([
				'Message' => $msg,
				'PhoneNumber' => $phone,
			]);
			// var_dump($result);
		} catch (AwsException $e) {
			// output error message if fails
			error_log($e->getMessage());
		} 
	}


	if(isset($_SERVER['HTTP_ORIGIN'])){
		header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
		header("Access-Control-Allow-Headers: Content-Type");
	}
	$login_fail = 0;
	$uid = 0;

	if(isset($_GET['rasaid'])) {
		$rasaid = (int)$_GET['rasaid'];
		$RASA_ENDPOINT = $RASA_ENDPOINTS[$rasaid];
	}

	//if($login_fail != 1 && isset($_POST['message'])){
		$booking_id = 0;
		if(isset($_POST["booking_id"])){
			$booking_id = $_POST["booking_id"];
		}else{
		}
		
		
		$mode = "view";

		if(isset($_POST["mode"])){
			$mode = $_POST["mode"];
		}

		$role = "";
		if(isset($_POST["role"])){
			$role = $_POST["role"];
		}

		$uid = "";
		if(isset($_POST["uid"])){
			$uid = $_POST["uid"];
		}

		$message = "";
		if(isset($_POST["message"])){
			$message = $_POST["message"];
		}

		//echo $role;
		$content = [];

		if($role==2000){
			//if case manager..

			$type = 0;
			$intent_id = null;
			$utter_id = null;
			$ai_id = 99;
			insertToDB($conn, $booking_id, $uid , null , $intent_id , $utter_id , $message,  $ai_id, $type, $role);
			//echo "[". json_encode($content) ."]";
			//TODO: output success.

		}else{
			// if patient .. 
			
			$user_input = $_POST['message'];

			if($mode=="view"){

				$pos = strpos($message, "我叫");

				if($pos){ //manual mode
					
					//greeting mode.
					$arr = explode("我叫", $message);
					if(isset($arr[1])){
						$name = $arr[1];
						$content["text"] = $name . "你好";
					}

				}else{
					
					$url = $RASA_ENDPOINT.'/model/parse';
					$ch = curl_init();
					$payload = json_encode( array( "text"=> $_POST['message'] ) );
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					$result = curl_exec($ch);
					curl_close($ch);
					$content = json_decode($result);
					$intent = $content->{'intent'}->{'name'};
					$confidence = $content->{'intent'}->{'confidence'};
	
					if ($confidence <= 0.1 ) {
						$ch2 = curl_init();
						$payload2 = json_encode( array( "sender"=> $uid, "message"=> "/restart" ) );
						curl_setopt($ch2, CURLOPT_URL, $url);
						curl_setopt($ch2, CURLOPT_POST, true);
						curl_setopt( $ch2, CURLOPT_POSTFIELDS, $payload2 );
						curl_setopt( $ch2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
						curl_setopt( $ch2, CURLOPT_RETURNTRANSFER, true );
						$result2 = curl_exec($ch2);
						curl_close($ch2);		
								
					}
	
					if ($intent == 'want_to_die' || $intent == 'want_to_die:') {
						$isKeyword = false;
						if($user_input=="我想死"){
							$isKeyword = true;
						}

						if($user_input=="呢個世界無人幫到我"){
							$isKeyword = true;

						}
						
						if($user_input=="我覺得自己好冇用"){

							$isKeyword = true;
						}
						
						if($user_input=="我冇晒希望啦"){

							$isKeyword = true;
						}
						
						if($user_input=="我死咗全世界人都會好開心"){

							$isKeyword = true;
						}
						
						if($user_input=="我死咗唔會再拖累人"){

							$isKeyword = true;
						}
						
						if($user_input=="我走咗可能啲人會開心啲呢"){

							$isKeyword = true;
						}
						
						if($user_input=="我想解脫￼￼￼￼￼￼￼￼"){

							$isKeyword = true;
						}
						
						if($user_input=="死咗就一了百了￼"){

							$isKeyword = true;
						}
						
						if($user_input=="人死左會係點㗎"){

							$isKeyword = true;
						}
						$result = $conn->query("select * from icase_user where uid = $uid LIMIT 1");
						$row = $result->fetch_array();
						$user_name = $row['name'];
						//$cm_phone = $row['phone'];
	
						$sql2 = "SELECT * FROM icase_user where uid = (SELECT cm_id FROM xover_ai.icase_patient_distribution where p_id = $uid LIMIT 1)";
						$result = $conn->query($sql2);
						//print($sql2);
						$row = $result->fetch_array();
						//$cm_id = $row['cm_id'];
						$phone = $row['phone'];
	
						//$result = $conn->query("select * from icase_user where uid = $cm_id LIMIT 1");
						//$row = $result->fetch_array();
	
						$msg = "Risk Mode detected for Patient
	Patient: ($user_name)
	reference no. ($uid)
	Risk Mode trigger:-
	\"$user_input\"
	PLEASE CONTACT PATIENT AS SOON AS POSSIBLE
	
	病人危機檢測
	病人 ($user_name) 參考號: ($uid)
	危機程式觸發關鍵字：
	\"$user_input\"
	請盡快與病人聯絡";
						if($isKeyword){
							send_sms($msg, $phone);
						}
					}
	
					$url = $RASA_ENDPOINT.'/webhooks/rest/webhook';
					$ch = curl_init();
					$payload = json_encode( array( "sender"=> $uid, "message"=> $_POST['message'] ) );
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					$result = curl_exec($ch);
					curl_close($ch);

					$result_content = str_replace("[","",$result);
					$result_content = str_replace("]","",$result_content);
					$result_content = str_replace("\ufffc","",$result_content);

					$content = json_decode($result_content);
		
					$content = json_decode(json_encode($content), True);
					
				}
			}

			//print('booking_id'. $booking_id);
			$content['booking_id'] = $booking_id;

			//print("booking_id2". $booking_id);
			$type = 0;
			
			if(isset($_POST["type"])){
				if($_POST["type"]=="idle"){
					$type = 1;
				}
			}
			
			$result = "";
			if(isset($content["text"])){
				$result = $content["text"];
			}

			insertToDB($conn, $booking_id, $uid , $message, $intent , $result, 99, $type, $role);

			echo "[". json_encode($content) ."]";
			
	}

	function insertToDB($conn, $booking_id, $sender_id, $content, $intent_id,  $response, $ai_id, $type, $role){
		$sql = "INSERT INTO `icase_chat_record` ( `booking_id`, `uid`, `content`, `intent_id`, `response`, `datetime`, `ai_id`, `type`, `role`) values('$booking_id', '$sender_id', '$content', '$intent_id',  '$response', curtime()  ,'$ai_id', $type, $role)";
		if($conn->query($sql)){
			
		}
		//var_dump($sql);
	}

	
?>