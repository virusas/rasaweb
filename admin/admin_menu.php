<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<!--
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
	  <div class="sidebar-brand-icon rotate-n-15">
	    <i class="fas fa-laugh-wink"></i>
	  </div>
	  <div class="sidebar-brand-text mx-3">i case manager</div>
	</a>-->
	<div style="margin:10px;">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="" style="">
		<img src="/rasaweb/img/logo.jpeg" width=200 height=140 >
		</a>
	</div>
	<!-- Divider -->
	<hr class="sidebar-divider my-0">
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?php if($_SERVER['REQUEST_URI'] == "/admin.php"){ echo "active";} ?>">
	  <a class="nav-link" href="admin.php">
	    <i class="fas fa-fw fa-tachometer-alt"></i>
	    <span>User</span></a>
	</li>
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?php if($_SERVER['REQUEST_URI'] == "/admin_distribution.php"){ echo "active";} ?>">
	  <a class="nav-link" href="admin_distribution.php">
	    <i class="fas fa-fw fa-tachometer-alt"></i>
	    <span>Distribution</span></a>
	</li>
	
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
	  <button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>
<!-- End of Sidebar -->