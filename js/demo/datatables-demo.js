// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable();
  $('#patient_dataTable').DataTable();
  $('#casemanager_dataTable').DataTable();
  $('#doctor_dataTable').DataTable();
  $('#admin_dataTable').DataTable();
});
