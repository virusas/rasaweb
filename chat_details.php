<?php
	session_start();
  require_once("DB_config.php");
  require_once("Role.php");
  require_once("checkLogin.php");

  function getUtter($conn,$text){
    $sql = "SELECT code FROM rasa_utter where text = '$text'";

    $result = $conn->query($sql);
    while($row = $result->fetch_array()){
      return $row["code"];
    }
  }

  function updateUtter($conn,$utter_id,$id){
    $sql = "UPDATE icase_chat_record a SET utter_id = (SELECT code FROM rasa_utter b where b.text = a.response) where id = '$id'";
    $conn->query($sql);
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iCase Manager - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  
    <link rel="apple-touch-icon" href="logo192.png" />
    <link rel="manifest" href="/manifest.json" />
    <link href="/static/css/2.fb64e31e.chunk.css" rel="stylesheet">
    <link href="/static/css/main.34de6062.chunk.css" rel="stylesheet">

</head>

<body id="page-top">
<div id="root"></div>
  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php require_once("caseman/caseman_menu.php"); ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $post_loginname; ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
						<?php

						?>
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Chat Record By : 
            <?php
              $booking_id = $_GET["booking_id"];
              $sql = "select *, b.loginname from booking_record a join icase_user b on a.pid = b.uid where a.id=". $booking_id;
                $result = $conn->query($sql);
              
              //var_dump($result);
              $startime = "";
              $datetime = "";
              if($result){
                while($row = $result->fetch_array()){
                  echo $row["loginname"];
                  //$startime = $row["start_datetime"];
                  $datetime = $row["booking_datetime"];
                }
              }
              
            ?>
              </h1>
              <br>
            <h1 class="h3 mb-0 text-gray-800">Time : 
              <?=$datetime?>
              </h1>
          </div>

          <div class="row">
            <!-- Pending Requests Card Example -->
            <div class="col-xl col-xl col-md mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1"><a href="/rasaweb/bot/generate_rasa.php" target="_blank">Start training(Chatbot will be temporaty unavailable.)</a></div>
                    </div>
                      <div class="col-auto">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#AddIntentModal">
                        </a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="row">
              <!-- Basic Card Example -->
              <div class="card shadow col">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-warning">Chat Record</h6>
                </div>
                <div class="card-body">
              		<div class="table-responsive">
                  	<form>
                		<table class="table table-bordered" id="patient_dataTable" width="100%" cellspacing="0">
                  		<thead>
                    		<tr>
                      		<th>User Message</th>
                      		<th>Reponse</th>
                      		<th>Time</th>
                    		</tr>
                  		</thead>
                  		<tbody>
                        <?php
                        //  $sql = "select * from icase_chat_record join  where permission = 1000 and sender_id in (select p_id from icase_patient_distribution where cm_id = $cm_id)";
                        $sql = "select * from icase_user a join icase_chat_record b on a.uid = b.uid where b.booking_id = $booking_id";
                        //print_r($sql);
                        $result = $conn->query($sql);

                          if($result){
                            while($row = $result->fetch_array()){
                                echo "<tr>";
                                if(!strpos($row['content'], "[") && !strpos($row['content'], "]")){
                                  echo "<td width=30%>".$row['content']."<br><input readonly=readonly style='width:100%;' value=".$row['intent_id']."></input><br><a target='_blank' href=intent.php?id=" . $row['id']. "&type=intent&user_msg=" .urlencode($row['content']). ">Change</a></td>";
                                }else{
                                  echo "<td width=30%></td>";
                                }
                                  $utter_id = "";
                                  if($row['utter_id']==null){
                                    $utter_id = getUtter($conn, $row['response']);
                                    updateUtter($conn, $utter_id, $row['id']);
                                  }else{
                                    $utter_id = $row['utter_id'];
                                  }
                                  echo "<td width=30%>".$row['response']."<br><input readonly=readonly  style='width:100%;' value=".$utter_id."></input><br><a  target='_blank' href=intent.php?id=" . $row['id']. "&type=response&intent_id=" . $utter_id . ">Change</a></td>";
                                  echo "<td>".$row['datetime']."</td>";
                                  //echo "<td><input type='submit' class='btn btn-primary start_dis' value='view history' data-toggle='modal' data-target='#PatientDistributionModal'></td>";
                                  echo "</tr>";
                                //}
                              }
                            }
                          
												?>
											</tbody>
										</table>
                    </form>
									</div>
                </div>
              </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
$(document).ready(function() {
  $('#patient_dataTable').DataTable();
  var x =document.getElementsByClassName("dataTables_wrapper");
  for (i = 0; i < x.length; i++) {
  	 x[i].style.width = "99%";
  }
});
  
  </script>

</body>

</html>

