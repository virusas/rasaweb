
<?php
  //session_start();
  require_once("DB_config.php");
  require_once("Role.php");
  //require_once("checkLogin.php");

  function getUtter($conn,$text){
    $sql = "SELECT code FROM rasa_utter where text = '$text'";

    $result = $conn->query($sql);
    while($row = $result->fetch_array()){
      return $row["code"];
    }
  }

  function updateUtter($conn,$utter_id,$id){
    $sql = "UPDATE icase_chat_record a SET utter_id = (SELECT code FROM rasa_utter b where b.text = a.response) where id = '$id'";
    $conn->query($sql);
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iCase Manager - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  
    <link rel="apple-touch-icon" href="logo192.png" />
    <link rel="manifest" href="/manifest.json" />
    <link href="/static/css/2.fb64e31e.chunk.css" rel="stylesheet">
    <link href="/static/css/main.34de6062.chunk.css" rel="stylesheet">

</head>

<body id="page-top">
<div id="root"></div>
  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php require_once("caseman/caseman_menu.php"); ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $post_loginname; ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
              <!-- Basic Card Example -->
              <div class="card shadow col">
                <div class="card-body">
              		
                </div>
<?php

    if(isset($_SERVER['HTTP_ORIGIN'])){
        header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        header("Access-Control-Allow-Headers: Content-Type");
    }

    $intents = array();

    $file_content = "";
    $last_code = "";
    $rasa_select_query = "";

    if(isset($_GET['rasaid'])) {
		$rasaid = (int)$_GET['rasaid'];
        $RASA_ENDPOINT = $RASA_ENDPOINTS[$rasaid];
        $rasa_select_query = "where rasaid = $rasaid";
	}

    $conn->query("set character_set_client='utf8'"); 
    $conn->query("set character_set_results='utf8'"); 
    $conn->query("set collation_connection='utf8_general_ci'");
    $result = $conn->query("select * from rasa_intent $rasa_select_query");
    $sql = "select * from rasa_intent $rasa_select_query";
    //print($sql);
    while($row = $result->fetch_array()){
        $code = $row['code'];
        $text = $row['text'];

        if ($code != $last_code) {
            array_push($intents, $code);

            if (!empty($last_code)) {
                $file_content .= "\n";
            }
            $last_code = $code;
            $file_content .= "## intent:$code\n";
        }
        $file_content .= "- $text\n";

    }

    //$fp = fopen('bot/nlu.md', 'w');
    //fwrite($fp, $file_content);
    //fclose($fp);
    $nlu_content = $file_content;
    // generated nlu.md

    $utters = array();

    $file_content = "";
    $last_code = "";

    $result = $conn->query("select * from rasa_utter $rasa_select_query");

    $file_content .= "intents:\n";
    foreach($intents as $intent) {
        $file_content .= "- $intent\n";
    }

    $file_content .= "templates:\n";
    while($row = $result->fetch_array()){
        $code = $row['code'];
        $text = $row['text'];

        if ($code != $last_code) {
            array_push($utters, $code);

            // if (!empty($last_code)) {
            //     $file_content .= "\n";
            // }
            $last_code = $code;
            $file_content .= "  $code:\n";
        }
        $file_content .= "  - text: $text\n";
    }

    $file_content .= "actions:\n";
    foreach($utters as $utter) {
        $file_content .= "- $utter\n";
    }

    //$fp = fopen('bot/domain.yml', 'w');
    //fwrite($fp, $file_content);
    //fclose($fp);
    $domain_content = $file_content;
    // generate domain.yml

    $file_content = "";
    $last_code = "";

    $result = $conn->query("select * from rasa_simple_story $rasa_select_query");

    // $file_content .= "intents:\n";
    // foreach($intents as $intent) {
    //     $file_content .= "- $intent\n";
    // }

    // $file_content .= "templates:\n";
    while($row = $result->fetch_array()){
        $intent_code = $row['intent_code'];
        $utter_code = $row['utter_code'];

        $file_content .= "## $intent_code\n";
        $file_content .= "* $intent_code\n";
        $file_content .= "    - $utter_code\n";
        $file_content .= "    - action_restart\n";
    }

    //$fp = fopen('bot/stories.md', 'w');
    //fwrite($fp, $file_content);
    //fclose($fp);
    $stories_content = $file_content;
    // generated stories.md


    //print("domain.".$domain_content);
    //print("nlu_content.".$nlu_content);
    //print("stories_content.".$stories_content);

    
    // $user_input = $_POST['message'];
    $url = $RASA_ENDPOINT.'/model/train';
    $ch = curl_init();
    $payload_data = array(
        "config" => "%YAML 1.1\n---\nlanguage: en\npipeline: supervised_embeddings\npolicies:\n- name: MemoizationPolicy\n- name: KerasPolicy\n- name: MappingPolicy\n- core_threshold: 0.2\n  fallback_action_name: action_default_fallback\n  name: FallbackPolicy\n  nlu_threshold: 0.1\n",
        "domain" => $domain_content,
        "nlu" => $nlu_content,
        "stories" => $stories_content,
        "force" => false,
        "save_to_default_model_directory" => true
    );


    //print_r($domain_content);
    $payload = json_encode( $payload_data );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //print_r($result);
    if ($httpcode == 200) {
        echo "Train successfullay!";
    } else {
        echo "Train fail!";
        // oops! have to handle it
        print_r($result);
    }
    $matches = [];

   
   //print($result);
    preg_match('/\d+-\d+\.tar/', $result, $matches);
    $model_file = $matches[0].".gz";
    // echo $result;
    // $content = json_decode($result);
    // $intent = $content->{'intent'}->{'name'};
    // $confidence = $content->{'intent'}->{'confidence'};

    // update model
    // oops! absolute path
    // $model_file = "/Users/efc/works/learn/rasaxx/models/20191212-214627.tar.gz";
    $model_file = "models/$model_file";

    $url = $RASA_ENDPOINT.'/model';
    $ch = curl_init();
    $payload_data = array(
        "model_file" => $model_file
    );
    $payload = json_encode( $payload_data );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    // curl_setopt($ch, CURLOPT_PUT, true);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($httpcode == 200 || $httpcode == 204) {
        echo "Update model";
    }
    else {
        // oops! have to handle it
    }

?>
</div>
</div>
</div>
</div>
</div>
<!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
  <div class="modal-footer">
    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
    <a class="btn btn-primary" href="logout.php">Logout</a>
  </div>
</div>
</div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/chart-area-demo.js"></script>
<script src="js/demo/chart-pie-demo.js"></script>

<!-- Page level plugins -->
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script>
$(document).ready(function() {
$('#patient_dataTable').DataTable();
var x =document.getElementsByClassName("dataTables_wrapper");
for (i = 0; i < x.length; i++) {
 x[i].style.width = "99%";
}
});

</script>

</body>

</html>