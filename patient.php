<?php
	session_start();
  require_once("DB_config.php");
  /*
  require "scss.inc.php";
  $scss = new scssc();
  echo $scss->compile('@import "scss/template.scss"');*/

  $id = NULL;
	if( isset($_SESSION['loginname']) && isset($_SESSION['password']) ){
		$post_loginname = $_SESSION['loginname'];
		$post_password = $_SESSION['password'];
		
		$sql = "select * from icase_user where loginname = '$post_loginname' and password = '$post_password'";
		$result = $conn->query($sql);
		$row_num = $result->num_rows;
		if($row_num == 1){
			$row = $result->fetch_array();
			if($row['permission'] != 1000){
				echo "<meta http-equiv='refresh' content='0;url=login.php' />";
			}else{
				$id = $row['uid'];
			}
		}
	}else{
		echo "<meta http-equiv='refresh' content='0;url=login.php' />";
  }

  require_once("patient/add_booking.php");
  
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iCase Manager - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <!--<link href="vendor/jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">-->
  <link href="vendor/jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">
  
    <link rel="apple-touch-icon" href="logo192.png" />
    <link rel="manifest" href="/manifest.json" />
    <!--
    <link href="static/css/prod/2.css" rel="stylesheet">
    <link href="static/css/prod/main.css" rel="stylesheet">
-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    
<!--<link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">-->
</head>

<body id="page-top">
<div id="root"></div>
  <!-- Page Wrapper -->
  <div id="wrapper">

		<?php require_once("patient/patient_menu.php"); ?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $post_loginname; ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Your Bookings</h1>
          </div>

          <div class="row">

						<?php
                          $sql = "select b.* from icase_user a join booking_record b on a.uid = b.pid where pid = '$id'";
                          
                          //print_r($sql);
                          $result = $conn->query($sql);
                          //if($result){
                            $row = mysqli_num_rows($result)

						?>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Bookings</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $row; ?></div>
                    </div>
                    <div class="col-auto">
                			<a class="dropdown-item" href="#" data-toggle="modal" data-target="#AddCaseManagerModal">
                      	<i class="fas fa-plus fa-2x text-gray-300"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-warning">Bookings</h6>
                </div>
                <div class="card-body">
              		<div class="table-responsive">
                		<table class="table table-bordered" id="patient_dataTable" width="100%" cellspacing="0">
                  		<thead>
                    		<tr>
                      		<th>ID</th>
                          <th>Booking Datetime</th>
                          <th></th>
                    		</tr>
                  		</thead>
                  		<tbody>
												<?php
                          if($result){
                    
                            while($row = $result->fetch_array()){

                            $minutes_to_add = 15;
                            date_default_timezone_set('Asia/Hong_Kong');

                            $starttime = new DateTime($row['booking_datetime']);
                            $endtime = new DateTime($row['booking_datetime']);

                            //$starttime->setTimezone(new DateTimeZone('Asia/Hong_Kong'));
                            //$endtime->setTimezone(new DateTimeZone('Asia/Hong_Kong'));
    
                            $endtime = $endtime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
    
                            $now = new \DateTime();
                            //$now->setTimezone(new DateTimeZone('Asia/Hong_Kong'));
    
                            $hasActiveChat = false;

                            echo "starttime".$starttime->format(\DateTime::ISO8601)."<br>";
                            echo "endtime". $endtime->format(\DateTime::ISO8601)."<br>";
                            echo "now". $now->format(\DateTime::ISO8601)."<br>";
                            if($starttime <= $now && $endtime >= $now){
                              $hasActiveChat = true;
                            }

                              echo "<tr>";
                              echo "<td>".$row['id']."</td>";
                              echo "<td>".$row['booking_datetime']."</td>";
                              //if($row['booking_datetime']){
                              if($hasActiveChat){
                                //can chat...
                                echo "<td><a href='chat.php'>Start Chat</a></td>";
                              }
                              //$stamp = $time->format('YYYY-MM-DD HH:mm');
                              //}
                              //echo "<td><input type='submit' class='btn btn-primary start_dis' value='view history' data-toggle='modal' data-target='#PatientDistributionModal'></td>";
                              echo "</tr>";
                            }
                          }
												?>
											</tbody>
										</table>
									</div>
                </div>
              </div>

        <!-- /.container-fluid -->
        

      </div>
      <!-- End of Main Content -->


    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- AddBookingModal-->
  <div class="modal fade" id="AddCaseManagerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Make New Booking</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form name="cm_form" action="" method="POST">
        <div class="modal-body">
          <div class="form-group row">
          	<div class="col-sm-6 mb-3 mb-sm-0">
            	<!--<input type="text" id="dt_picker" name="booking_time" class="form-control form-control-user" placeholder="Booking Time" required>-->
              <div class='input-group' style="width:400px;" >
                    <input type='text' class="form-control" id='dt_picker' name="dt"/>
                    <input type='hidden' name="id" value="<?=$id?>" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Add">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/bootstrap/js/moment.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<!--<script type="text/javascript" src="vendor/bootstrap/js/transition.js"></script>-->


  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
$(document).ready(function() {
  $('#patient_dataTable').DataTable();
  var x =document.getElementsByClassName("dataTables_wrapper");
  for (i = 0; i < x.length; i++) {
  	 x[i].style.width = "99%";
  } 
            $(function () {
                $('#dt_picker').datetimepicker({
                inline: true,
                sideBySide: true,
                format: 'YYYY-MM-DD HH:mm'
                });
            });
  /*
      $('#dt_picker').datetimepicker({
        format:'unixtime'
      });*/
});

  </script>

</body>

</html>
