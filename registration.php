<!DOCTYPE html>
<html lang="en">
<?php
  require_once("DB_config.php");?>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Register</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script type='text/javascript'>
    function check(){
      $isValid = true;
      $checkingString = "Please enter ";
      var $isFirst = true;
      if(!$('#repeatPassword').val()){
        if(!$isFirst){
          $checkingString += " , ";
        }
          $isFirst = false;
        $isValid = false;
        $checkingString += "repeatPassword";
      }
      if(!$('#name').val()){
        if(!$isFirst){
          $checkingString += " , ";
        }
          $isFirst = false;
        $isValid = false;
        $checkingString += "name";
      }
      if(!$('#gender').val()){
        if(!$isFirst){
          $checkingString += " , ";
        }
          $isFirst = false;
        $isValid = false;
        $checkingString += "gender";
      }
      if(!$('#age').val()){
        if(!$isFirst){
          $checkingString += " , ";
        }
          $isFirst = false;
        $isValid = false;
        $checkingString += "age";
      }
      if(!$('#phone').val()){
        if(!$isFirst){
          $checkingString += " , ";
        }
        $isFirst = false;
        $isValid = false;
        $checkingString += "phone";
      }
      if($('#password').val() != $('#repeatPassword').val()){
        $isValid = false;
        //$checkingString += "password";
        $checkingString = "the passwords does not match";
      }
      if(!$isValid){
        alert($checkingString);
      }
      return $isValid;
    }</script>
  <?php
    $loginname = "";
		$gender = "";
		$age = "";
    $phone = "";
		$name = "";
    $password = "";
    $cm = "";
    $permission = 1000;
    
    if(isset($_GET['cm'])){
      $cm = htmlspecialchars($_GET['cm']);
    }
    if(isset($_POST['cm'])){
      $cm = htmlspecialchars($_POST['cm']);
    }
    if(isset($_POST['loginname'])){

      $loginname = htmlspecialchars($_POST['loginname']);
    }
    if(isset($_POST['gender'])){
      
		  $gender = htmlspecialchars($_POST['gender']);
    }
    if(isset($_POST['age'])){
      $age = htmlspecialchars($_POST['age']);
      
    }
    if(isset($_POST['phone'])){
      
      $phone = htmlspecialchars($_POST['phone']);
    }
    if(isset($_POST['name'])){
      $name = htmlspecialchars($_POST['name']);
      
    }
    if(isset($_POST['password'])){
	  	$password = htmlspecialchars($_POST['password']);
      
    }
    if(isset($_POST['gender'])){
	  	$gender = htmlspecialchars($_POST['gender']);
      
    }
		$permission = 1000;

	if(isset($_POST['loginname']) && isset($_POST['gender']) && isset($_POST['age']) && isset($_POST['phone'])  && isset($_POST['name']) && isset($_POST['password'])){
    /*$loginname = htmlspecialchars($_POST['loginname']);
		$gender = htmlspecialchars($_POST['gender']);
		$age = htmlspecialchars($_POST['age']);
    $phone = htmlspecialchars($_POST['phone']);
		$name = htmlspecialchars($_POST['name']);
		$password = htmlspecialchars($_POST['password']);
    $permission = 1000;*/

    $sql = "select * from `icase_user` where loginname = '". $_POST['loginname'] . "'";

		$result = $conn->query($sql);
		$row_num = $result->num_rows;
    printSQL($sql, $debug);
		if($row_num >= 1){
      ?> 
      <script>alert('user already exist.'); </script>
      <?php
    }else{
      $encrypedPW = hash('sha512', htmlspecialchars($_POST['password']));
      $sql = "INSERT INTO `icase_user`(`loginname`, `password`, `permission`, `name`, `gender`, `age`, `phone`) VALUES
       ('".$loginname."','".$encrypedPW."','".$permission."','".$name."','".$gender."','".$age."','".$phone."');";
        
		  if($conn->query($sql) === TRUE){
        $last_id = $conn->insert_id;

        if($last_id){
          //TODO : get doctor id

          $sql = "INSERT INTO `icase_patient_distribution`(`p_id`, `cm_id`, `dr_id`) VALUES ($last_id,$cm,$cm)";
          //$sql = "update icase_patient_distribution set cm_id = '$cm_id' , dr_id = '$dr_id' where p_id = '$pa_id'";
          $conn->query($sql);
          printSQL($sql, $debug);
          echo "<meta http-equiv='refresh' content='0;url=login.php' />";

        }


        //header("Location:login.php");
        //echo "Success";
        //$row_num = $result->num_rows;
        //if($row_num == 1){
          //$row = $result->fetch_array();
          //$_SESSION['loginname'] = $loginname;
          //$_SESSION['password'] = $password;
         // $_SESSION['permission'] = $permission;
         // setcookie("rememberMe", "true", time()+3600*24*30); //30 days;
          //if($row['permission'] == 1000){
          //}
        //}
      }
      else
      {
        //echo "Error";
  
      }
    }
	}
  ?>

  </script>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
               <form class="user" method="POST" onsubmit="return check()">
                <div class="form-group">
                  <input type="loginname" class="form-control form-control-user" id="loginname" name="loginname" placeholder="Login name" value="<?=$loginname?>">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="password" name="password"  placeholder="Password"  value="<?=$password?>">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="repeatPassword"  name="repeatPassword" placeholder="Repeat Password"  value="<?=$password?>">
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="Name"  value="<?=$name?>">
                </div>  
                <div class="form-group">
                  <!--<input type="select" class="form-control form-control-user" id="exampleGender" placeholder="gender">-->
                      <select id="gender" name="gender"  class="form-control form-control-user">
                      <option value="">Gender</option>
                      <option value="M" <?php if($gender=="M"){ echo "selected"; }?>>Male</option>
                      <option value="F" <?php if($gender=="F"){ echo "selected"; }?>>Female</option>
                    </select>
                </div>  
                <div class="form-group">
                  <input type="age" class="form-control form-control-user" id="age" name="age" placeholder="Age"  value="<?=$age?>">
                </div>  
                <div class="form-group">
                  <input type="phone" class="form-control form-control-user" id="phone" name="phone" placeholder="Phone"  value="<?=$phone?>">
                </div>  
                <input type="hidden" id="cm" name="cm" value="<?=$cm?>">

                <input type="submit" class="btn btn-primary btn-user btn-block">
              </form>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
