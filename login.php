<?php
	session_start();
  require_once("DB_config.php");

  $rememberMe = false;
  /*
    foreach ($_POST as $key => $value) {
      echo "<tr>";
      echo "<td>";
      echo $key;
      echo "</td>";
      echo "<td>";
      echo $value;
      echo "</td>";
      echo "</tr>";
  }
  */
  //setcookie("remember_me", $remember_me, time()+3600*24*30, '/');

  $hasAlert = false;
	if( isset($_POST['loginname']) && isset($_POST['password']) ){
    $post_loginname = htmlspecialchars($_POST['loginname']);
    if(isset($_POST['rememberMe'])){
     // $remember_me =  'On';
      $rememberMe = true;
    }
    
		$post_password = hash('sha512', htmlspecialchars($_POST['password']));
		
    $sql = "select * from icase_user where loginname = '$post_loginname' and password = '$post_password'";
    
		$result = $conn->query($sql);
		$row_num = $result->num_rows;
		if($row_num == 1){
			$row = $result->fetch_array();
			$_SESSION['loginname'] = $post_loginname;
			$_SESSION['password'] = $post_password;
      $_SESSION['permission'] = $row['permission'];
      setcookie("rememberMe", time() - 3600);

      if($rememberMe){
        setcookie("rememberMe", "true", time()+3600*24*30); //30 days;
        //print("route1");
       // $rememberMe = true;
       
      }else{
        setcookie("rememberMe", "false", 0); //0 days;
        //print("route2");
      //  $rememberMe = false;
      }
      //print("rememberMe".$_COOKIE['rememberMe']);

			if($row['permission'] == 1000){
        //echo "<meta http-equiv='refresh' content='0;url=booking.php' />";
        header("Location:booking.php");
			}elseif($row['permission'] == 2000){
				echo "<meta http-equiv='refresh' content='0;url=caseman.php' />";
			}elseif($row['permission'] == 3000){
				echo "<meta http-equiv='refresh' content='0;url=caseman.php' />";
			}elseif($row['permission'] == 4000){
				echo "<meta http-equiv='refresh' content='0;url=admin.php' />";
			}
		}else{
      $hasAlert = true;
    }
	}elseif( isset($_SESSION['loginname']) && isset($_SESSION['password']) ){
		$post_loginname = htmlspecialchars($_SESSION['loginname']);
		$post_password = hash('sha512', htmlspecialchars($_SESSION['password']));
		
		$sql = "select * from icase_user where loginname = '$post_loginname' and password = '$post_password'";
		$result = $conn->query($sql);
		$row_num = $result->num_rows;
		if($row_num == 1){
			$row = $result->fetch_array();
			$_SESSION['loginname'] = $post_loginname;
			$_SESSION['password'] = $post_password;
      $_SESSION['permission'] = $row['permission'];

      if($rememberMe=='On'){
        $remenberMe = true;

      }else{
        setcookie('remember_me',$remember_me, 9999999, '/');//0 days;
        $remenberMe = false;

      }
      
			if($row['permission'] == 1000){
				echo "<meta http-equiv='refresh' content='0;url=booking.php' />";
				//echo "<meta http-equiv='refresh' content='0;url=http://18.163.35.189:3000/' />";
			}elseif($row['permission'] == 2000){
				echo "<meta http-equiv='refresh' content='0;url=caseman.php' />";
			}elseif($row['permission'] == 3000){
				echo "<meta http-equiv='refresh' content='0;url=caseman.php' />";
			}elseif($row['permission'] == 4000){
				echo "<meta http-equiv='refresh' content='0;url=admin.php' />";
			}
		}
	}else{}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iCase Manager - Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <?php 
    //$hasAlert = true;
    if($hasAlert){
      ?>
      <script>
        alert('login fail');
      </script>
      <?php
    }
  ?>
</head>

<body class="bg-gradient-primary">
  <div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <img src="./img/logo.jpeg" height=211 width=300 >
                  </div>
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">歡迎回來！</h1>
                  </div>
                  <form class="user" method="POST">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="exampleInputLoginname" name="loginname" placeholder="帳號" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="exampleInputPassword" name="password" placeholder="密碼" required>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck" name="rememberMe" value="ok" <?php if($rememberMe){ echo " checked='checked'"; } ?> />
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-user btn-block" value="登入"/>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
