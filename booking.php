<?php
	session_start();
  require_once("config.php");
  require_once("DB_config.php");
  require_once("Role.php");
  require_once("checkLogin.php");
  require_once("patient/add_booking.php");


  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iCase Manager - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  
    <link rel="apple-touch-icon" href="logo192.png" />
    <!--<link rel="manifest" href="/manifest.json" />-->

  <link href="vendor/jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">
  
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  

    <script>

    function setCookie(name,value,minutes) {
    var expires = "";
    if (minutes) {
        var date = new Date();
        date.setTime(date.getTime() + (minutes*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
      document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {   
        document.cookie = name+'=; Max-Age=-99999999;';  
    }

    function go(bId,uid,mode,role,gender,starttime){
          setCookie("booking_id", bId, 15);
          setCookie("uid", uid, 15);
          setCookie("mode", mode, 15);
          setCookie("role", role, 15);
          setCookie("gender", gender, 15);                      
          //window.open("<?= $CHATBOT_ENDPOINT ?>","_targetWindow","width=800,height=600");

          if(starttime != ''){
            $('#starttime').val(starttime);
           // alert(starttime);
          }

          if(bId != ''){
            $('#booking_id').val(bId);
            //alert('bid : '+bId);
          }else{
            //alert('no');
          }

          $('#booking_form').submit();
    }
    function iOSversion() {

      if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        if (!!window.indexedDB) { return 'iOS 8 and up'; }
        if (!!window.SpeechSynthesisUtterance) { return 'iOS 7'; }
        if (!!window.webkitAudioContext) { return 'iOS 6'; }
        if (!!window.matchMedia) { return 'iOS 5'; }
        if (!!window.history && 'pushState' in window.history) { return 'iOS 4'; }
        return true;
      }

      return false;
    }

      <?php 

      
        if(!isset($_POST["starttime"]) || $_POST["starttime"] == ""){
           // $starttime = $_POST["starttime"];
           //route1
            $booking_id = null;
            if(isset($_POST["booking_id"])){

              $booking_id = $_POST["booking_id"];
              $sqlxx = "update booking_record set booking_starttime = now() where id = ". $booking_id;
              $conn->query($sqlxx);
              $remainSeconds = 15*60;
              ?>
               setCookie("remaintime", <?=$remainSeconds?>, 15);  
              // window.location.href = <?= $CHATBOT_ENDPOINT ?>;); 
              //  window.open("http://localhost:3000/");
              //if(iOSversion()){
                //window.location.href = '<?= $CHATBOT_ENDPOINT ?>';
              // window.location.replace('<?= $CHATBOT_ENDPOINT ?>');

                window.location.replace("<?= $CHATBOT_ENDPOINT ?>");  

              //}else{
                //window.open("<?= $CHATBOT_ENDPOINT ?>","_targetWindow","width=800,height=600");
              //}
              // var windowReference = window.open();

              //windowReference.location = <?= $CHATBOT_ENDPOINT ?>;

              //alert('ok');

               <?php
            }
        }else{
          //route2
          if($_POST["booking_id"]!=""){
            $starttime = $_POST["starttime"];
            //$date1Timestamp = strtotime($starttime);
            $date2Timestamp = strtotime("now");
  
            $difference = 0;
            $difference = $date2Timestamp - $starttime;
            //if($date1Timestamp != null && $date2Timestamp != null ){
            //}
            $remainSeconds = 15*60 - $difference;
            ?>
              setCookie("remaintime", <?=$remainSeconds?>, 15);
              //if(iOSversion()){
                window.location.href = '<?= $CHATBOT_ENDPOINT ?>';
              //}else{
              //  window.open("<?= $CHATBOT_ENDPOINT ?>","_targetWindow","width=800,height=600");
              //}
              //window.open("<?= $CHATBOT_ENDPOINT ?>");
             // alert('ok');
             //  var windowReference = window.open();

             // window.location.href = '<?= $CHATBOT_ENDPOINT ?>';
             // windowReference.location = <?= $CHATBOT_ENDPOINT ?>;
              <?php
            //}
          }
        }
      ?>
     </script>
</head>

<body id="page-top">
<div id="root"></div>
  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
            printSQL($sql, $debug);
    if($role == Role::CaseManager || $role == Role::Doctor){
      require_once("caseman/caseman_menu.php"); 
    }else if($role == Role::Paitent){
      require_once("patient/patient_menu.php"); 
    }
    ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

            <form id="booking_form" name="booking_form" action="" method="POST">
              <input type="hidden" id="booking_id" name="booking_id" value="" >
              <input type="hidden" id="starttime" name="starttime" value="" >
            </form>
        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $post_loginname; ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Your Bookings</h1>
          </div>

          <div class="row">

						<?php
              $sql = "select  * , b.id as bookingId,  booking_datetime, DATE_FORMAT(b.booking_datetime, '%Y-%m-%d')  Date , DATE_FORMAT(b.booking_datetime,'%H:%i:%s') Time from icase_user a join booking_record b on a.uid = b.pid where pid = '$id' order by date desc";
              //print("role".$role);
              if($role == Role::CaseManager){
                $sql = "select  *, b.id as bookingId,  booking_datetime,  DATE_FORMAT(b.booking_datetime, '%Y-%m-%d')  Date , DATE_FORMAT(b.booking_datetime,'%H:%i:%s') Time from icase_user a join booking_record b on a.uid = b.pid where a.permission = 1000 and a.uid in (select p_id from icase_patient_distribution where cm_id = $id) order by booking_datetime desc";
              }
              if($role == Role::Doctor){
                $sql = "select  *, b.id as bookingId,  booking_datetime,  DATE_FORMAT(b.booking_datetime, '%Y-%m-%d')  Date , DATE_FORMAT(b.booking_datetime,'%H:%i:%s') Time from icase_user a join booking_record b on a.uid = b.pid where a.permission = 1000 and a.uid in (select p_id from icase_patient_distribution where cm_id in (select cm_id from icase_patient_distribution where dr_id = $id)) order by booking_datetime desc";
              }
              printSQL($sql, $debug);

              $result = $conn->query($sql);

              $row = mysqli_num_rows($result)
              

						?>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Bookings</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $row; ?></div>
                    </div>
                    <?php if($role == Role::Paitent){?>
                      <div class="col-auto">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#AddCaseManagerModal">
                          <i class="fas fa-plus fa-2x text-gray-300"></i>
                        </a>
                      </div>
                    <?php }?>
                  </div>
                </div>
              </div>
            </div>
          </div>
              <?php if($role == Role::Paitent){?>
              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-warning">Bookings</h6>
                </div>
                <div class="card-body">
              		<div class="table-responsive">
                		<table class="table table-bordered" id="patient_dataTable" width="100%" cellspacing="0">
                  		<thead>
                    		<tr>
                      		<th>ID</th>
                      		<th>Date</th>
                      		<th>Time</th>
                      		<th></th>
                    		</tr>
                  		</thead>
                  		<tbody>
												<?php
                          if($result){
                            while($row = $result->fetch_array()){
                              
                              $starttime = new DateTime($row['booking_datetime']);
                              $endtime = new DateTime($row['booking_datetime']);

                              if($row['booking_starttime'] != null && $row['booking_starttime'] != ''){
                                $starttime = new DateTime($row['booking_starttime']);
                                $endtime = new DateTime($row['booking_starttime']);
                              }
                              
                              $endtime = $endtime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                              $strEndTime =  date_format($endtime, 'Y-m-d H:i:s');
      
                              $now = new \DateTime();
                              //$now->setTimezone(new DateTimeZone('Asia/Hong_Kong'));
      
                              $hasActiveChat = false;
                              //echo "starttime".$starttime->format(\DateTime::ISO8601)."<br>";
                              //echo "endtime". $endtime->format(\DateTime::ISO8601)."<br>";
                              //echo "now". $now->format(\DateTime::ISO8601)."<br>";
                              if($starttime <= $now && $endtime >= $now){
                                $hasActiveChat = true;
                              }
                              echo "<tr>";
                                echo "<td>".$row['bookingId']."</td>";
                                echo "<td>".$row['Date']."</td>";
                                echo "<td>".$row['Time']."</td>";
                              if($hasActiveChat){
                                $difference = 0;
                                $date1Timestamp = strtotime($row['booking_starttime']);
                                $date2Timestamp = strtotime("now");
                                if($date1Timestamp != null && $date2Timestamp != null ){
                                  $difference = $date2Timestamp - $date1Timestamp;
                                }
                                $remainSeconds = 15*60 - $difference;
                                ?><td><div style="display:none;"><?="date1Timestamp".$date1Timestamp?><?="date2Timestamp".$date2Timestamp?><?="different".$difference?><?="endtime".$strEndTime;?></div><a href="javascript:go(<?=$row['bookingId']?>,<?=$row['uid']?>,'view',<?=$role?>, '<?=$row['gender']?>', '<?=$date1Timestamp?>');">Join</a></td><?php
                              }else{
                                echo "<td><div style='display:none;'>".$strEndTime."</div></td>";
                              }
                              echo "</tr>";
                            }
                          }
												?>
											</tbody>
										</table>
									</div>
                </div>
              </div>
              
              <?php }else if($role == Role::CaseManager || $role == Role::Doctor) {?>

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-warning">Bookings</h6>
                </div>
                <div class="card-body">
              		<div class="table-responsive">
                		<table class="table table-bordered" id="patient_dataTable" width="100%" cellspacing="0">
                  		<thead>
                    		<tr>
                      		<th>Booking ID</th>
                      		<th>Patient ID</th>
                      		<th>Patient Name</th>
                      		<th>Date</th>
                      		<th>Time</th>
                      		<th></th>
                    		</tr>
                  		</thead>
                  		<tbody>
												<?php
                          if($result){
                            while($row = $result->fetch_array()){

                              $starttime = new DateTime($row['booking_datetime']);
                              $endtime = new DateTime($row['booking_datetime']);

                              if($row['booking_starttime'] != null && $row['booking_starttime'] != ''){

                                $starttime = new DateTime($row['booking_starttime']);
                                $endtime = new DateTime($row['booking_starttime']);

                                //$booking_datetime_diff = $starttime - $endtime; 

                              }
                              
                              $endtime = $endtime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
      
                              $now = new \DateTime();
                              //$now->setTimezone(new DateTimeZone('Asia/Hong_Kong'));
      
                              $hasActiveChat = false;

                              //echo "starttime".$starttime->format(\DateTime::ISO8601)."<br>";
                              //echo "endtime". $endtime->format(\DateTime::ISO8601)."<br>";
                              //echo "now". $now->format(\DateTime::ISO8601)."<br>";

                              if($starttime <= $now && $endtime >= $now){
                                $hasActiveChat = true;
                              }
                              echo "<tr>";
                              echo "  <td>".$row['bookingId']."</td>";
                              echo "  <td>".$row['uid']."</td>";
                              echo "  <td>".$row['name']."</td>";
                              echo "  <td>".$row['Date']."</td>";
                              echo "  <td>".$row['Time']."</td>";
                              if($hasActiveChat){
                                $difference = 0;
                                $date1Timestamp = strtotime($row['booking_starttime']);
                                $date2Timestamp = strtotime("now");
                                if($date1Timestamp != null && $date2Timestamp != null ){
                                  $difference = $date2Timestamp - $date1Timestamp;
                                }
                                $remainSeconds = 15*60 - $difference;
                                ?><td><!--<?="x".$date1Timestamp?><?="y".$date2Timestamp?>--><a href="javascript:go(<?=$row['bookingId']?>,<?=$row['uid']?>,'view',<?=$role?>, '<?=$row['gender']?>', '<?=$date1Timestamp?>');">Join</a></td><?php
                              }
                              echo "<td></td>";
                              //echo "<td><input type='submit' class='btn btn-primary start_dis' value='view history' data-toggle='modal' data-target='#PatientDistributionModal'></td>";
                              echo "</tr>";
                            }
                          }
												?>
											</tbody>
										</table>
									</div>
                </div>
              </div>  
            <?php } ?>
        <!-- /.container-fluid -->
        

      </div>
      <!-- End of Main Content -->


    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- AddBookingModal-->
  <div class="modal fade" id="AddCaseManagerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Booking</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form name="cm_form" action="" method="POST">
        <div class="modal-body">
          <!--
        	<div class="form-group">
          	<input type="text" name="cm_loginname" class="form-control form-control-user" placeholder="Login Name" required>
          </div>
          -->
          <div class="form-group row">
          	<!--<div class="col-sm-6 mb-3 mb-sm-0">
            </div>-->
            <form name="cm_form" action="" method="POST">
              <div class='input-group' style="width:400px;" >
                  <input type="text" id="dt_picker" name="dt" class="form-control form-control-user" placeholder="Booking Time" required>
                  <input type='hidden' name="id" value="<?=$id?>" />
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Add">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins
  <script src="vendor/chart.js/Chart.min.js"></script> -->

  <!-- Page level custom scripts 
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>-->

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/bootstrap/js/moment.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
$(document).ready(function() {
  $('#patient_dataTable').DataTable( {
        "order": [[ 1, "desc" ]]
    } );
  var x =document.getElementsByClassName("dataTables_wrapper");
  for (i = 0; i < x.length; i++) {
  	 x[i].style.width = "99%";
  }
});

            $(function () {
                $('#dt_picker').datetimepicker({
                inline: true,
                sideBySide: true,
                format: 'YYYY-MM-DD HH:mm'
                });
            });
  </script>

</body>

</html>
