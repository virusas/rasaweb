<?php
require __DIR__ . '/vendor/autoload.php';
require_once("local_settings.php");

$servername = $DB_HOST;
$username = $DB_USERNAME;
$password = $DB_PASSWORD;
$dbname = $DB_NAME;

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);
$conn->query("set character_set_client='utf8'");
 $conn->query("set character_set_results='utf8'");
  $conn->query("set collation_connection='utf8_general_ci'");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$debug = false;

if(isset($_GET['debug'])){
    $debug = true;
}
function printSQL($sql, $debug){
    if($debug){
        print($sql);
    }
}
?>